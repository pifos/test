package s12596.mpr.user.db

import java.util.List;

import s12596.mpr.user.module.Role;

public interface RoleRepository extends Repository<Role> {
	public List<Role> withName(String name, PagingInfo page);
}