package s12596.mpr.user.db;

import java.util.*;

import s12596.mpr.user.module.Adres;

public interface AddressRepository extends Repository<Address> {
	public List<Address> withCountry(String country, PagingInfo page);
	public List<Address> withCity(String city, PagingInfo page);
	public List<Address> withStreet(String street, PagingInfo page);
}