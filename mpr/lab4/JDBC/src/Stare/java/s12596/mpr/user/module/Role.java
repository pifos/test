package s12596.mpr.user.module;

import java.util.List;

public class Role {

	private int id;
	private String name;
	private int idUser;
	private int idPermission;
	private List<user> users;
	private List<Permission> permissions;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<user> getUsers() {
		return users;
	}
	public void setUsers(List<user> users) {
		this.users = users;
	}
	public List<Permission> getPermissions() {
		return permissions;
	}
	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public int getIdPermission() {
		return idPermission;
	}
	public void setIdPermission(int string) {
		this.idPermission = string;
	}
	
	
	
}
